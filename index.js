
	
	// 1.  Create a function which will be able to add two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of the addition in our console.
	// 	-function should only display result. It should not return anything.
		
		function sum(num){
	console .log('Displayed sum of 5 and 15')
	console.log (num)
}
sum(5 + 15)


	// 	Create a function which will be able to subtract two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of subtraction in our console.
	// 	-function should only display result. It should not return anything.
		
		function diff(num){
	console .log('Displayed difference of 20 and 5')
	console.log (num)
}
diff(20 - 5)

	// 	-invoke and pass 2 arguments to the addition function
	// 	-invoke and pass 2 arguments to the subtraction function

	// 2.  Create a function which will be able to multiply two numbers.
	// 		-Numbers must be provided as arguments.
	// 		-Return the result of the multiplication.

			function product(num){

				return num;
			}
			console.log('The product of 50 and 10:')
			
			console.log(product(50 * 10))

	// 	Create a function which will be able to divide two numbers.
	// 		-Numbers must be provided as arguments.
	// 		-Return the result of the division.
			function quotient(num){

			return num;
			}
			console.log('The quotient of 50 and 10:')
			console.log(quotient(50 / 10))

	//  	Create a global variable called outside of the function called product.
	// 		-This product variable should be able to receive and store the result of multiplication function.
	// 	Create a global variable called outside of the function called quotient.
	// 		-This quotient variable should be able to receive and store the result of division function.

	// 	Log the value of product variable in the console.
	// 	Log the value of quotient variable in the console.

	// 3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
	// 		-a number should be provided as an argument.
	// 		-look up the formula for calculating the area of a circle with a provided/given radius.
	// 		-look up the use of the exponent operator.
	// 		-you can save the value of the calculation in a variable.
	// 		-return the result of the area calculation.

	// 	Create a global variable called outside of the function called circleArea.
	// 		-This variable should be able to receive and store the result of the circle area calculation.

	// Log the value of the circleArea variable in the console.
			function circleArea(num){

			return num;
			}
			console.log('The result of getting the area of a circle with 15 radius:')
			
			console.log(circleArea(3.14159*15*15))


	// 4. 	Create a function which will be able to get total average of four numbers.
	// 		-4 numbers should be provided as an argument.
	// 		-look up the formula for calculating the average of numbers.
	// 		-you can save the value of the calculation in a variable.
	// 		-return the result of the average calculation.

	//     Create a global variable called outside of the function called averageVar.
	// 		-This variable should be able to receive and store the result of the average calculation
	// 		-Log the value of the averageVar variable in the console.
			function averageVar(num){

			return num;
			}
			console.log('The average of 20,40,60 and 80:')
			let add = (20 + 40 + 60 + 80)
			let average = (add / 4)
			console.log(average)
	

	// 5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
	// 		-this function should take 2 numbers as an argument, your score and the total score.
	// 		-First, get the percentage of your score against the total. You can look up the formula to get percentage.
	// 		-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
	// 		-return the value of the variable isPassed.
	// 		-This function should return a boolean.

	// 	Create a global variable called outside of the function called isPassingScore.
	// 		-This variable should be able to receive and store the boolean result of the checker function.
	// 		-Log the value of the isPassingScore variable in the console.
			
// 			function checkDivisibilityBy8(num){
// 			let remainder = num %8;
// 			console.log	('The remainder of ' + num + 'divided by 8 is: ' + remainder);
// 			let isDivisibleby8 = remainder === 0;
// 			console.log('Is ' + num + ' divisible by 8?');
// 			console .log(isDivisibleby8);
// 			} 
// 			checkDivisibilityBy8(64);
// 			checkDivisibilityBy8(28);
// -----------------------------------------------------------
			function isPassingScore(num){

			return isPassingScore;
			}
			let score = 25
			console.log	('Is 38/50 a passing score?');
			console.log(25 == 25)